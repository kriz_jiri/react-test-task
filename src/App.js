import React, { Component } from 'react';
import './App.css';
import Table from "./components/table";
import data from './data/data.js';


class App extends Component {
  render() {
    return (
      <Table tableRows={data}/>
    );
  }
}

export default App;
