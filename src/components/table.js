import React, {Component} from 'react';
import TableRow from "./table_row";


class Table extends Component {
    constructor(props) {
        super(props);
        this.state = {rows: this.props.tableRows}
        console.log(this.state.rows);
    }

    deleteRow(deleteRowID) {
        const rows = this.state.rows;

        for (let i=0; i<rows.length; i++) {
            const rowID = Object.values(rows[i].data)[0];

            if(rowID === deleteRowID) {
                rows.splice(i, 1);
                this.setState({rows: rows});
                break;
            }
        }
    }

    renderTableHeader() {
        const headerValues = Object.keys(this.state.rows[0].data);

        return headerValues.map((value, index) => {
            if(index === 0) {
                return <th colSpan="2">{value}</th>
            }
            return <th>{value}</th>
        });
    }

    renderRows() {
        return this.state.rows.map(row => {
            return <TableRow key={Object.values(row.data)[0]}
                             row={row}
                             deleteRow={(selectedRow) => this.deleteRow(selectedRow)}/>
        });
    }

    render() {

        return (
            <table className="table table-striped table-bordered">
                <thead>
                    <tr>
                        {this.renderTableHeader()}
                        <th></th>
                    </tr>
                </thead>
                {this.renderRows()}
            </table>
        );
    }
}


export default Table;
