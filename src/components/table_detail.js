import React, { Component } from 'react';
import TableRowRelatives from "./table_row_relatives";

class TableDetail extends Component {

    renderTableRowRelatives() {
        if (Object.keys(this.props.relatives).length !== 0) {

            const records = this.props.relatives.has_relatives.records;
            return records.map(function (value, index) {
                return <TableRowRelatives key={index} row={value}/>
            });
        } else {
            return [];
        }
    }

    render() {
        return (
            <tr>
                <td colSpan="15">
                    <table className="Table table-striped table-bordered">
                        <thead>
                            <tr><td colSpan={15}>has_relatives</td></tr>
                                <tr>
                                    <th colSpan={2}>Relative ID</th>
                                    <th>Patient ID</th>
                                    <th>Is Alive?</th>
                                    <th>Frequency of visits</th>
                                </tr>
                        </thead>
                        {this.renderTableRowRelatives()}
                    </table>
                </td>
            </tr>
        )
    }
}

export default TableDetail;