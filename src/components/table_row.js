import React, { Component } from 'react';
import TableDetail from './table_detail';

class TableRow extends Component {


    constructor(props) {
        super(props);
        this.state = {clicked: false,};
        this.onRowClick = this.onRowClick.bind(this);
        this.deleteRow = this.deleteRow.bind(this);
    }

    renderTableDetail(){
        return <TableDetail relatives={this.props.row.kids}/>
    }

    renderColumns() {
        const data = Object.values(this.props.row.data);

        return data.map((client, index) => {
            return <td key={index}>{client}</td>;
        });
    }

    onRowClick() {
        this.setState(prevState => ({
            clicked: !prevState.clicked
        }));
    }

    deleteRow() {
        const row_id = Object.values(this.props.row.data)[0];
        this.props.deleteRow(row_id);
    }

    render() {
        return (
            <tbody>
                <tr>
                    <td>
                        {this.state.clicked ? (
                            <span className="glyphicon glyphicon-chevron-down"
                                    onClick={this.onRowClick}>
                            </span>
                        ) : (
                            <span className="glyphicon glyphicon-chevron-right"
                                    onClick={this.onRowClick}>
                            </span>
                        )}
                    </td>
                    {this.renderColumns()}
                    <td>
                        <span className="glyphicon glyphicon-remove" onClick={this.deleteRow} aria-hidden="true"></span>
                    </td>
                </tr>
                {this.state.clicked && this.renderTableDetail()}
            </tbody>
        );
    }
}

export default TableRow;