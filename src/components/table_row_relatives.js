import React, { Component } from 'react';
import TablePhones from './table_phones'

class TableRowRelatives extends Component {


    constructor(props) {
        super(props);

        this.state = {clicked: false};

        this.onRowClick = this.onRowClick.bind(this);
    }

    onRowClick() {
        this.setState(prevState => ({
            clicked: !prevState.clicked
        }));
    }

    renderZaznam() {
        const data = Object.values(this.props.row.data);

        return data.map((client, index) => {
            return <td key={index}>{client}</td>;
        });
    }

    render() {
        return (
            <tbody>
                <tr>
                    <td>
                        {this.state.clicked ? (
                            <span className="glyphicon glyphicon-chevron-down"
                                  onClick={this.onRowClick}>
                                </span>
                        ) : (
                            <span className="glyphicon glyphicon-chevron-right"
                                  onClick={this.onRowClick}>
                                </span>
                        )}
                    </td>
                    {this.renderZaznam()}
                </tr>

                {this.state.clicked &&
                <tr>
                    <td colSpan={15}>
                        <TablePhones kids={this.props.row.kids}/>
                    </td>
                </tr>
                }
            </tbody>
        );
    }
}

export default TableRowRelatives;