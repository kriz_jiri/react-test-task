import React, { Component } from 'react';
import TableRowPhones from './table_row_phones';

class TablePhones extends Component {

    renderTableRowPhones() {
        if (Object.keys(this.props.kids).length !== 0) {
            const data = this.props.kids.has_phone.records;
            return data.map(function (value, index) {
                return <TableRowPhones key={index} data={value}></TableRowPhones>
            });

        } else {
            return []
        }
    }

    render() {
        return(<table className="Table table-striped table-bordered">
                <thead>
                    <tr><td colSpan={15}>has_phones</td></tr>
                    <tr>
                        <th>Phone ID</th>
                        <th>ID of the relative</th>
                        <th>Phone</th>
                    </tr>
                </thead>
                    <tbody>
                        {this.renderTableRowPhones()}
                    </tbody>
                </table>);
    }
}

export default TablePhones;