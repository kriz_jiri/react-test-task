import React, { Component } from 'react';

class TableRowPhones extends Component {


    constructor(props) {
        super(props);

        this.state = {clicked: false};

        this.onRowClick = this.onRowClick.bind(this);
    }

    renderRow () {
        const data = Object.values(this.props.data.data);
        return data.map((client, index) => {
            return <td key={index}>{client}</td>;
        });
    }

    onRowClick() {
        this.setState(prevState => ({
            clicked: !prevState.clicked
        }));
    }

    render() {
        return (
            <tr>
                {this.renderRow()}
            </tr>
        );
    }
}

export default TableRowPhones;